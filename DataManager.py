from pymongo import MongoClient


client = MongoClient()
db = client.douban_imdb_rating


def beautify(r):
    br = {}
    br['movie_name_cn'] = r['movie_name_cn']
    br['movie_douban_score'] = r['movie_douban_score']
    br['imdb_score'] = r['imdb_score'][0]
    return br


def get_info_by_imdb(imdb_id):
    if imdb_id:
        result = db.all_data.find(
                    {"movie_imdb_url": "http://www.imdb.com/title/{}".format(imdb_id)},
                    {
                        "_id": 0,
                        "movie_name_cn": 1,
                        "movie_douban_score": 1,
                        "imdb_score": 1,},
                    )
        if result.count():
            return beautify(result[0])
        else:
            return None


def get_info_by_douban(douban_id):
    if douban_id:
        result = db.all_data.find(
                    {"movie_douban_url": "https://movie.douban.com/subject/{}/".format(douban_id)},
                    {
                        "_id": 0,
                        "movie_name_cn": 1,
                        "movie_douban_score": 1,
                        "imdb_score": 1,

                    })
        if result.count():
            return beautify(result[0])
        else:
            return None
