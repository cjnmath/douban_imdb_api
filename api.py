from flask import Flask
from flask_restful import Resource, Api, reqparse
from DataManager import get_info_by_douban, get_info_by_imdb

app = Flask(__name__)
api = Api(app)


parser = reqparse.RequestParser()
parser.add_argument('search_field')
parser.add_argument('id')


class get_info(Resource):
    def get(self, search_field, id):
        if search_field == "douban":
            print("GET Request: get info by douban_id <{}>".format(id))
            return get_info_by_douban(id), 200
        if search_field == "imdb":
            print("GET Request get info by imdb:", id)
            return get_info_by_imdb(id), 200

    def post(self):
        args = parser.parse_args()
        if args['search_field'] == 'douban':
            print("POST Request: get info by douban_id <{}>".format(args['id']))
            return get_info_by_douban(args['id']), 200
        if args['search_field'] == 'imdb':
            print("POST Request: get info by imdb_id <{}>".format(args['id']))
            return get_info_by_imdb(args['id']), 200


api.add_resource(get_info, "/", "/<search_field>/<id>")

if __name__ == '__main__':
    print("Starting the job!")
    app.run(host="0.0.0.0", port=5000, debug=True)
    # app.run(debug=True)
